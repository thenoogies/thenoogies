spam = ["cat", "dog", "bird", "mouse"]
print(spam[0])

# Adding Values to Lists with the append() and insert() Methods
spam1 = ['cat', 'dog', 'bat']
spam1.append('moose')
print(spam1)

spam2 = ['cat', 'dog', 'bat']
spam2.insert(1, 'chicken')
print(spam2)

# Removing Values from Lists with remove()
spam3 = ['cat', 'bat', 'rat', 'elephant']
spam3.remove('bat')
print(spam3)

# Sorting the Values in a List with the sort() Method
spam4 = [2, 5, 3.14, 1, -7]
spam4.sort()
print(spam4)

spam5 = ['ants', 'cats', 'dogs', 'badgers', 'elephants']
spam5.sort()
print(spam5)

# The Tuple Data Type
# In parenetheses () not square brackets [] lists are [].
# Tuples cannot have their values modified, appended, or removed.

type(('hello',))
print(type)

type(('hello'))
print(type)

# The copy Module’s copy() and deepcopy() Functions
import copy
spam = ['A', 'B', 'C', 'D']
cheese = copy.copy(spam)
cheese[1] = 42
print(spam)
print(cheese)