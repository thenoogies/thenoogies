print('--------')
print('IF CLAUSE')
print('--------')
name = 'Steve'

if name == 'Maria':
    print('Hi, Maria.')
else:
    print('Go away! You are not Maria!.')

print('--------')
print('ELIF CLAUSE')
print('--------')

name = 'Steve'
age = 32
if name == 'Maria':
    print('Hi, Maria.')
elif age < 31:
    print('Hey little kid, you are not Maria.')
elif age > 100:
    print('Liar!')
else:
    print('Right, seriously you are not Maria.')

print('--------')
print('WHILE LOOP CLAUSE')
print('--------')

spam = 0
if spam < 5:
    print('Hello, world.')
    spam = spam + 1

spam = 0
while spam < 5:
    print('Hello, world.')
    spam = spam + 1

print('--------')
print('ANNOYING WHILE LOOP CLAUSE')
print('--------')

name = ''
while name != 'your name':
    print('Please type your name.')
    name = input()
print('Thankyou!')

print('--------')
print('BREAKING ANNOYING WHILE LOOP CLAUSE')
print('--------')

while True:
    print('Please type your name.')
    name = input()
    if name == 'your name':
        break
print('Thank you.')
